import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCuzp9zrwPkKOHHcQOtN98N50a_0ezDWEk",
    authDomain: "notesapp-63171.firebaseapp.com",
    projectId: "notesapp-63171",
    storageBucket: "notesapp-63171.appspot.com",
    messagingSenderId: "910262768417",
    appId: "1:910262768417:web:cf42b05029be94aefa6460",
    measurementId: "G-Y16NQE5QMK"
  };

if (!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
}

export {firebase};